# coding: utf-8
import logging, requests, os
from typing import Dict, List, Union  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Header
)

from openapi_server.models.extra_models import TokenModel  # noqa: F401
from openapi_server.models.juice_db import JuiceDB
from openapi_server.models.juice_offering import JuiceOffering
from openapi_server.models.juice_offering_db import JuiceOfferingDB
from openapi_server.models.juice_request import JuiceRequest
from openapi_server.models.juice_request_db import JuiceRequestDB
from openapi_server.db import database

router = APIRouter()

@router.get(
    "/foodbank/user/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def foodbank_user(
    x_yivi_user: Union[str, None] = Header(default=None)
) -> None:
    return {"user": x_yivi_user, "logOutUrl": os.getenv("LOGOUT_URL")}

@router.post(
    "/foodbank/acceptJuiceRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_accept_juice_request(
    body: str = Body(None, description="The uuid of the juiceRequest."),
) -> None:
    """Notify the food bank that juice can be delivered."""
    database.juice_requests[body] = {
        "juiceRequest": database.juice_requests[body]["juiceRequest"],
        "state": "ACCEPTED"
    }
    return {"description": "Message delivered."}

@router.post(
    "/foodbank/rejectJuiceRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_reject_juice_request(
    body: str = Body(None, description="The uuid of the juiceRequest."),
) -> None:
    """Notify the food bank that juice can't be delivered."""
    print(body)
    database.juice_requests[body] = {
        "juiceRequest": database.juice_requests[body]["juiceRequest"],
        "state": "NOT AVAILABLE"
    }
    print(database.juice_requests[body])
    return {"description": "Message delivered."}

@router.post(
    "/foodbank/juiceOfferings/",
    responses={
        200: {"description": "Successfully notified the foodbank."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_juice_offering_post(
    juice_offering: JuiceOffering = Body(None, description="The juice containing at least a name, a quantity and a price."),
) -> None:
    """Notify the foodbank that juices are available."""
    database.juice_offerings[juice_offering.id] = {
        "juiceOffering": juice_offering.dict(),
        "state": "OFFERED"
    }
    return {"description": "Successfully notified the foodbank."}


@router.get(
    "/foodbank/juiceOfferings/",
    responses={
        200: {"model": List[JuiceOfferingDB], "description": "List of all available juice offerings."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_juice_offerings_get(
) -> List[JuiceOfferingDB]:
    """Get list of available juice offerings."""
    return database.get_database_juice_offerings()


@router.post(
    "/foodbank/juiceRequests/",
    responses={
        200: {"description": "Successfully notified the mobile press."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_juice_request(
    juice_request: JuiceRequest = Body(None, description="The juice containing at least a name, a quantity and a price."),
) -> None:
    """Ask for juice at the mobile press."""
    url = f"{os.getenv('FORWARD')}/press/juiceRequests/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = juice_request.dict()

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:press",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.juice_requests[juice_request.id] = {
            "juiceRequest": juice_request.dict(),
            "state": "REQUESTED"
        }
        return {"description": "Successfully notified the mobile press."}
    return {"Error": resp.text}


@router.get(
    "/foodbank/juiceRequests/",
    responses={
        200: {"model": List[JuiceRequestDB], "description": "List of juice requests of Food Banks."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_juice_requests_get(
) -> List[JuiceRequestDB]:
    """List of juice requests."""
    return database.get_database_juice_requests()


@router.get(
    "/foodbank/juices/",
    responses={
        200: {"model": List[JuiceDB], "description": "List of all available juices."},
    },
    tags=["Food Bank"],
    response_model_by_alias=True,
)
async def foodbank_juices_get(
) -> List[JuiceDB]:
    """Get list of available juices."""
    return database.get_database_juices()
