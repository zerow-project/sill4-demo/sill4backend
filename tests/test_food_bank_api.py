# coding: utf-8
import pytest
from fastapi.testclient import TestClient


from openapi_server.models.juice_db import JuiceDB  # noqa: F401
from openapi_server.models.juice_offering_db import JuiceOfferingDB  # noqa: F401
from openapi_server.models.juice_request import JuiceRequest  # noqa: F401
from openapi_server.models.juice_request_db import JuiceRequestDB  # noqa: F401

def test_juice_request(client: TestClient):
    """Test case for juice_request

    
    """
    juice_request = {
        "id": pytest.juice_request_id,
        "juices": [{
            "name": "Apple juice",
            "quantity": 30,
            "expiry_date": "2025-08-14"
        }]
    }

    response = client.request(
        "POST",
        "api/foodbank/juiceRequests/",
        json=juice_request,
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200


def test_juice_requests_get(client: TestClient):
    """Test case for juice_requests_get

    
    """

    response = client.request(
        "GET",
        "api/foodbank/juiceRequests/",
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["juiceRequest"]["id"] == pytest.juice_request_id

def test_juices_post(client: TestClient):
    """Test case for juices_post

    
    """
    juice_offering = {
        "id": pytest.juice_offering_id,
        "juices": [{
            "name": "Apple juice",
            "quantity": 30,
            "expiry_date": "2025-08-14"
        }]
    }
    response = client.request(
        "POST",
        "api/foodbank/juiceOfferings/",
        json=juice_offering,
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200

def test_juice_offerings_get(client: TestClient):
    """Test case for juice_offerings_get

    
    """

    response = client.request(
        "GET",
        "api/foodbank/juiceOfferings/",
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["juiceOffering"]["id"] == pytest.juice_offering_id
