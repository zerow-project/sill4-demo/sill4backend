import pytest
import uuid
from fastapi import FastAPI
from fastapi.testclient import TestClient

from openapi_server.main import app as application


@pytest.fixture
def app() -> FastAPI:
    application.dependency_overrides = {}

    return application


@pytest.fixture
def client(app) -> TestClient:
    return TestClient(app)

def pytest_configure():
    pytest.fruit_id = str(uuid.uuid4())
    pytest.fruit_offer_id = str(uuid.uuid4())
    pytest.farmer_fruit_request_id = str(uuid.uuid4())
    pytest.press_fruit_request_id = str(uuid.uuid4())
    pytest.juice_request_id = str(uuid.uuid4())
    pytest.juice_offering_id = str(uuid.uuid4())