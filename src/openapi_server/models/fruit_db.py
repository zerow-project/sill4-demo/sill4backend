# coding: utf-8

from __future__ import annotations
from datetime import date, datetime  # noqa: F401

import re  # noqa: F401
from typing import Any, Dict, List, Optional  # noqa: F401

from pydantic import AnyUrl, BaseModel, EmailStr, Field, validator  # noqa: F401
from openapi_server.models.fruit import Fruit


class FruitDB(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.

    FruitDB - a model defined in OpenAPI

        id: The id of this FruitDB.
        fruit: The fruit of this FruitDB.
    """

    id: str = Field(alias="id")
    fruit: Fruit = Field(alias="fruit")

FruitDB.update_forward_refs()
