import json
from openapi_server.models.fruit import Fruit
from openapi_server.models.fruit_db import FruitDB
from openapi_server.models.fruit_offering_db import FruitOfferingDB
from openapi_server.models.juice_offering_db import JuiceOfferingDB
from openapi_server.models.fruit_request_db import FruitRequestDB
from openapi_server.models.juice_request_db import JuiceRequestDB
class Database:
    fruits = dict()
    fruit_offerings = dict()
    juice_offerings = dict()
    fruit_requests = dict()
    juice_requests = dict()

    def get_database_fruits(self):
        return [FruitDB(**self.fruits[key]) for key in self.fruits]
    
    def get_database_fruit_offerings(self):
        return [FruitOfferingDB(**self.fruit_offerings[key]) for key in self.fruit_offerings]

    def get_database_juice_offerings(self):
        return [JuiceOfferingDB(**self.juice_offerings[key]) for key in self.juice_offerings]
    
    def get_database_fruit_requests(self):
        for key in self.fruit_requests:
            print(self.fruit_requests[key])
        return [FruitRequestDB(**self.fruit_requests[key]) for key in self.fruit_requests]

    def get_database_juice_requests(self):
        return [JuiceRequestDB(**self.juice_requests[key]) for key in self.juice_requests]
database = Database()