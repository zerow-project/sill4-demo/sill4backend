# coding: utf-8
import pytest
from fastapi.testclient import TestClient


from openapi_server.models.fruit_db import FruitDB  # noqa: F401
from openapi_server.models.fruit_offering import FruitOffering  # noqa: F401
from openapi_server.models.fruit_offering_db import FruitOfferingDB  # noqa: F401
from openapi_server.models.fruit_request import FruitRequest  # noqa: F401
from openapi_server.models.juice_offering_db import JuiceOfferingDB  # noqa: F401
from openapi_server.models.juice_request import JuiceRequest  # noqa: F401
from openapi_server.models.juice_request_db import JuiceRequestDB  # noqa: F401


def test_fruit_offering_post(client: TestClient):
    """Test case for fruit_offering_post

    
    """
    fruit_offering = {
        "offer_id": pytest.fruit_offer_id,
        "fruits": [{"name": "Apples", "quantity": 20, "price": 0.50, "expiry_date": "2023-6-20"}]
    }
    response = client.request(
        "POST",
        "api/press/fruitOfferings/",
        json=fruit_offering,
    )

    assert response.status_code == 200


def test_fruit_offerings_get(client: TestClient):
    """Test case for fruit_offerings_get

    
    """
    response = client.request(
        "GET",
        "api/press/fruitOfferings/"
    )

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["id"] == pytest.fruit_offer_id


def test_fruit_request(client: TestClient):
    """Test case for fruit_request

    
    """
    fruit_request_obj = {
        "id": pytest.press_fruit_request_id,
        "fruits": [
            {
                "quantity": 200,
                "price": 0.72,
                "name": "Apples",
                "expiry_date": "2023-08-20"
            }
        ]
    }

    response = client.request(
        "POST",
        "api/press/fruitRequests/",
        json=fruit_request_obj
    )

    assert response.status_code == 200

def test_fruit_requests_get(client: TestClient):
    """Test case for fruit_requests_get

    
    """

    response = client.request(
        "GET",
        "api/press/fruitRequests/"
    )

    assert response.status_code == 200
    assert len(response.json()) == 2
    assert response.json()[1]["fruit_request"]["id"] == pytest.press_fruit_request_id

def test_accept_fruits_post(client: TestClient):
    """Test case for accept_fruit_request

    
    """
    response = client.request(
        "POST",
        "api/press/acceptFruitRequest/",
        json=pytest.press_fruit_request_id
    )

    assert response.status_code == 200

def test_accept_fruits_post(client: TestClient):
    """Test case for accept_fruits_post

    
    """
    response = client.request(
        "POST",
        "api/press/acceptFruits/",
        json=pytest.fruit_offer_id
    )

    assert response.status_code == 200


def test_juice_request(client: TestClient):
    """Test case for juice_request

    
    """
    juice_request = {
        "id": pytest.juice_request_id,
        "juices": [{
            "name": "Apple juice",
            "quantity": 30,
            "expiry_date": "2025-08-14"
        }]
    }

    response = client.request(
        "POST",
        "api/press/juiceRequests/",
        json=juice_request,
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200


def test_juice_requests_get(client: TestClient):
    """Test case for juice_requests_get

    
    """
    response = client.request(
        "GET",
        "api/press/juiceRequests/",
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["juiceRequest"]["id"] == pytest.juice_request_id


def test_juices_post(client: TestClient):
    """Test case for juices_post

    
    """
    juice_offering = {
        "id": pytest.juice_offering_id,
        "juices": [{
            "name": "Apple juice",
            "quantity": 30,
            "expiry_date": "2025-08-14"
        }]
    }
    response = client.request(
        "POST",
        "api/press/juiceOfferings/",
        json=juice_offering,
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == 200

