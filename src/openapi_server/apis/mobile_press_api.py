# coding: utf-8
import logging, requests, os
from typing import Dict, List, Union  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Header
)

from openapi_server.models.extra_models import TokenModel  # noqa: F401
from openapi_server.models.fruit_db import FruitDB
from openapi_server.models.fruit_offering import FruitOffering
from openapi_server.models.fruit_offering_db import FruitOfferingDB
from openapi_server.models.fruit_request import FruitRequest
from openapi_server.models.fruit_request_db import FruitRequestDB
from openapi_server.models.juice_offering import JuiceOffering
from openapi_server.models.juice_offering_db import JuiceOfferingDB
from openapi_server.models.juice_request import JuiceRequest
from openapi_server.models.juice_request_db import JuiceRequestDB
from openapi_server.db import database


router = APIRouter()

@router.get(
    "/press/user/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def press_user(
    x_yivi_user: Union[str, None] = Header(default=None)
) -> None:
    return {"user": x_yivi_user, "logOutUrl": os.getenv("LOGOUT_URL")}

@router.post(
    "/press/acceptFruitRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_accept_fruit_request(
    body: str = Body(None, description="The uuid of the fruitRequest."),
) -> None:
    """Notify the mobile press that the fruits can be delivered."""
    database.fruit_requests[body] = {
        "fruit_request": database.fruit_requests[body]["fruit_request"],
        "state": "ACCEPTED"
    }
    return {"description": "Message delivered."}

@router.post(
    "/press/rejectFruitRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_reject_fruit_request(
    body: str = Body(None, description="The uuid of the fruitRequest."),
) -> None:
    """Notify the mobile press that the fruits can't be delivered."""
    database.fruit_requests[body] = {
        "fruit_request": database.fruit_requests[body]["fruit_request"],
        "state": "NOT AVAILABLE"
    }
    return {"description": "Message delivered."}

@router.post(
    "/press/acceptFruits/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_accept_fruits_post(
    body: str = Body(None, description="The uuid of the fruitOffering"),
) -> None:
    """Notify the farmer that the fruits are accepted."""
    url = f"{os.getenv('FORWARD')}/farmer/acceptFruits/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:farmer",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.fruit_offerings[body] = {
            "id": body,
            "fruitOffering": database.fruit_offerings[body]["fruitOffering"],
            "state": "ACCEPTED"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}

@router.post(
    "/press/rejectFruits/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_reject_fruits_post(
    body: str = Body(None, description="The uuid of the fruitOffering"),
) -> None:
    """Notify the farmer that the fruits are rejected."""
    url = f"{os.getenv('FORWARD')}/farmer/rejectFruits/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:farmer",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.fruit_offerings[body] = {
            "id": body,
            "fruitOffering": database.fruit_offerings[body]["fruitOffering"],
            "state": "NOT AVAILABLE"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}


@router.post(
    "/press/acceptJuiceRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_accept_juice_request(
    body: str = Body(None, description="The uuid of the juiceRequest."),
) -> None:
    """Notify the food bank that juice can be delivered."""
    url = f"{os.getenv('FORWARD')}/foodbank/acceptJuiceRequest/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:foodbank",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.juice_requests[body] = {
            "juiceRequest": database.juice_requests[body]["juiceRequest"],
            "state": "ACCEPTED"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}

@router.post(
    "/press/rejectJuiceRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_reject_juice_request(
    body: str = Body(None, description="The uuid of the juiceRequest."),
) -> None:
    """Notify the food bank that juice can be delivered."""
    url = f"{os.getenv('FORWARD')}/foodbank/rejectJuiceRequest/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:foodbank",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.juice_requests[body] = {
            "juiceRequest": database.juice_requests[body]["juiceRequest"],
            "state": "NOT AVAILABLE"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}

@router.post(
    "/press/fruitOfferings/",
    responses={
        200: {"description": "Successfully notified the mobile press operator."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_fruit_offering_post(
    fruit_offering: FruitOffering = Body(None, description="The fruit containing at least a name, a quantity and a price."),
) -> None:
    """Notify the mobile press that fruits are available."""
    database.fruit_offerings[fruit_offering.offer_id] = {
        "id": fruit_offering.offer_id,
        "fruitOffering": fruit_offering.dict(),
        "state": "OFFERED"
    }
    return {"description": "Successfully notified the mobile press operator."}


@router.get(
    "/press/fruitOfferings/",
    responses={
        200: {"model": List[FruitOfferingDB], "description": "List of fruit offerings of Farmers."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_fruit_offerings_get(
) -> List[FruitOfferingDB]:
    """List of fruit offerings."""
    return database.get_database_fruit_offerings()


@router.post(
    "/press/fruitRequests/",
    responses={
        200: {"description": "Successfully notified the farmer."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_fruit_request(
    fruit_request: FruitRequest = Body(None, description="The fruit containing at least a name, a quantity and a price."),
) -> None:
    """Ask for fruit at the farmer."""
    url = f"{os.getenv('FORWARD')}/farmer/fruitRequests/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = fruit_request.dict()

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:farmer",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.fruit_requests[fruit_request.id] = {
            "fruit_request": fruit_request.dict(),
            "state": "REQUESTED"
        }
        return {"description": "Successfully notified the farmer."}
    return {"Error": resp.text}


@router.get(
    "/press/fruitRequests/",
    responses={
        200: {"model": List[FruitRequestDB], "description": "List of fruit requests of Food Banks."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_fruit_requests_get(
) -> List[FruitRequestDB]:
    """List of fruit requests."""
    return database.get_database_fruit_requests()


@router.post(
    "/press/juiceOfferings/",
    responses={
        200: {"description": "Successfully notified the foodbank."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_juice_offering_post(
    juice_offering: JuiceOffering = Body(None, description="The juice containing at least a name, a quantity and a price."),
) -> None:
    """Notify the foodbank that juices are available."""
    url = f"{os.getenv('FORWARD')}/foodbank/juiceOfferings/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = juice_offering.dict()

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:foodbank",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.juice_offerings[juice_offering.id] = {
            "id": juice_offering.id,
            "juiceOffering": juice_offering.dict(),
            "state": "OFFERED"
        }
        return {"description": "Successfully notified the foodbank."}
    return {"Error": resp.text}

@router.get(
    "/press/juiceOfferings/",
    responses={
        200: {"model": List[JuiceOfferingDB], "description": "List of all available juice offerings."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_juice_offerings_get(
) -> List[JuiceOfferingDB]:
    """Get list of available juice offerings."""
    return database.get_database_juice_offerings()


@router.post(
    "/press/juiceRequests/",
    responses={
        200: {"description": "Successfully notified the mobile press."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_juice_request(
    juice_request: JuiceRequest = Body(None, description="The juice containing at least a name, a quantity and a price."),
) -> None:
    """Ask for juice at the mobile press."""
    database.juice_requests[juice_request.id] = {
        "juiceRequest": juice_request,
        "state": "REQUESTED"
    }
    return {"description": "Successfully notified the mobile press."}


@router.get(
    "/press/juiceRequests/",
    responses={
        200: {"model": List[JuiceRequestDB], "description": "List of juice requests of Food Banks."},
    },
    tags=["Mobile Press"],
    response_model_by_alias=True,
)
async def press_juice_requests_get(
) -> List[JuiceRequestDB]:
    """List of juice requests."""
    return database.get_database_juice_requests()
