# coding: utf-8
import pytest
from fastapi.testclient import TestClient


from openapi_server.models.fruit_db import FruitDB  # noqa: F401
from openapi_server.models.fruit_offering import FruitOffering  # noqa: F401
from openapi_server.models.fruit_offering_db import FruitOfferingDB  # noqa: F401
from openapi_server.models.fruit_request import FruitRequest  # noqa: F401


def test_fruits_post(client: TestClient):
    """Test case for fruits_post

    
    """
    fruit = [{
            "id": pytest.fruit_id,
            "fruit" : {"name": "Apples", "quantity": 20, "price": 0.50, "expiry_date": "2023-6-20"}
    }]
    response = client.request(
        "POST",
        "api/farmer/fruits/",
        json=fruit,
    )
    assert response.status_code == 200

def test_fruits_get(client: TestClient):
    """Test case for fruits_get

    
    """
    response = client.request(
        "GET",
        "api/farmer/fruits/",
    )

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["id"] == pytest.fruit_id

def test_fruit_offering_post(client: TestClient):
    """Test case for fruit_offering_post

    
    """
    response = client.request(
        "GET",
        "api/farmer/fruits/",
    )

    fruit = response.json()[0]

    fruit_offering = {
        "offer_id": pytest.fruit_offer_id,
        "fruits": [fruit["fruit"]]
    }
    response = client.request(
        "POST",
        "api/farmer/fruitOfferings/",
        json=fruit_offering,
    )

    assert response.status_code == 200

def test_fruit_offerings_get(client: TestClient):
    """Test case for fruit_offerings_get

    
    """

    response = client.request(
        "GET",
        "api/farmer/fruitOfferings/"
    )

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["id"] == pytest.fruit_offer_id


def test_fruit_request(client: TestClient):
    """Test case for fruit_request

    
    """
    fruit_request= {
        "id": pytest.farmer_fruit_request_id,
        "fruits": [
            {
                "quantity": 200,
                "price": 0.72,
                "name": "Apples",
                "expiry_date": "2023-08-20"
            }
        ]
    }

    response = client.request(
        "POST",
        "api/farmer/fruitRequests/",
        json=fruit_request
    )

    assert response.status_code == 200

def test_fruit_requests_get(client: TestClient):
    """Test case for fruit_offerings_get

    
    """

    response = client.request(
        "GET",
        "api/farmer/fruitRequests/"
    )

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["fruit_request"]["id"] == pytest.farmer_fruit_request_id

def test_accept_fruit_request(client: TestClient):
    """Test case for accept_fruit_request

    
    """
    response = client.request(
        "POST",
        "api/farmer/acceptFruitRequest/",
        json=pytest.farmer_fruit_request_id
    )
    print(response.text)
    assert response.status_code == 200


def test_accept_fruits_post(client: TestClient):
    """Test case for accept_fruits_post

    
    """
    response = client.request(
        "POST",
        "api/farmer/acceptFruits/",
        json=pytest.fruit_offer_id
    )

    assert response.status_code == 200







