# coding: utf-8

from __future__ import annotations
from datetime import date, datetime  # noqa: F401

import re  # noqa: F401
from typing import Any, Dict, List, Optional  # noqa: F401

from pydantic import AnyUrl, BaseModel, EmailStr, Field, validator  # noqa: F401


class Juice(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.

    Juice - a model defined in OpenAPI

        name: The name of this Juice.
        quantity: The quantity of this Juice.
        expiry_date: The expiry_date of this Juice.
    """

    name: str = Field(alias="name")
    quantity: float = Field(alias="quantity")
    expiry_date: str = Field(alias="expiry_date")

Juice.update_forward_refs()
