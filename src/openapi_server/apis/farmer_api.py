# coding: utf-8
import logging
import requests
import os
from typing import Dict, List, Union  # noqa: F401

from fastapi import (  # noqa: F401
    APIRouter,
    Body,
    Header
)

from openapi_server.models.extra_models import TokenModel  # noqa: F401
from openapi_server.models.fruit_db import FruitDB
from openapi_server.models.fruit_offering import FruitOffering
from openapi_server.models.fruit_offering_db import FruitOfferingDB
from openapi_server.models.fruit_request import FruitRequest
from openapi_server.models.fruit_request_db import FruitRequestDB
from openapi_server.db import database

router = APIRouter()


@router.get(
    "/farmer/user/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_user(
    x_yivi_user: Union[str, None] = Header(default=None)
) -> None:
    return {"user": x_yivi_user, "logOutUrl": os.getenv("LOGOUT_URL")}

@router.post(
    "/farmer/acceptFruitRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_accept_fruit_request(
    body: str = Body(None, description="The uuid of the fruitRequest."),
) -> None:
    """Notify the mobile press that the fruits can be delivered."""
    url = f"{os.getenv('FORWARD')}/press/acceptFruitRequest/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:press",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.fruit_requests[body] = {
            "fruit_request": database.fruit_requests[body]["fruit_request"],
            "state": "ACCEPTED"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}

@router.post(
    "/farmer/rejectFruitRequest/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_reject_fruit_request(
    body: str = Body(None, description="The uuid of the fruitRequest."),
) -> None:
    """Notify the mobile press that the fruits can't be delivered."""
    url = f"{os.getenv('FORWARD')}/press/rejectFruitRequest/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = body

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:press",
        "Forward-Sender": sender_id
    })
    
    if resp.status_code == 200:
        database.fruit_requests[body] = {
            "fruit_request": database.fruit_requests[body]["fruit_request"],
            "state": "NOT AVAILABLE"
        }
        return {"description": "Message delivered."}
    return {"Error": resp.text}

@router.post(
    "/farmer/acceptFruits/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_accept_fruits_post(
    body: str = Body(None, description="The uuid of the fruitOffering"),
) -> None:
    """Notify the farmer that the fruits are accepted."""
    database.fruit_offerings[body] = {
        "id": body,
        "fruitOffering": database.fruit_offerings[body]["fruitOffering"],
        "state": "ACCEPTED"
    }
    return {"description": "Message delivered"}

@router.post(
    "/farmer/rejectFruits/",
    responses={
        200: {"description": "Message delivered."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_reject_fruits_post(
    body: str = Body(None, description="The uuid of the fruitOffering"),
) -> None:
    """Notify the farmer that the fruits are rejected."""
    database.fruit_offerings[body] = {
        "id": body,
        "fruitOffering": database.fruit_offerings[body]["fruitOffering"],
        "state": "NOT AVAILABLE"
    }
    return {"description": "Message delivered"}

@router.post(
    "/farmer/fruitOfferings/",
    responses={
        200: {"description": "Successfully notified the mobile press operator."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruit_offering_post(
    fruit_offering: FruitOffering = Body(None, description="The fruit containing at least a name, a quantity and a price."),
) -> None:
    """Notify the mobile press that fruits are available."""
    url = f"{os.getenv('FORWARD')}/press/fruitOfferings/"
    sender_id = f"{os.getenv('SENDER_ID')}"
    body = fruit_offering.dict()

    print(f"Sending {body} to {url} with sender id: {sender_id} ")
    resp = requests.post(url, json=body, headers={
        "Forward-Id": "urn:zerow:press",
        "Forward-Sender": sender_id
    })

    if resp.status_code == 200:
        database.fruit_offerings[fruit_offering.offer_id] = {
            "id": fruit_offering.offer_id,
            "fruitOffering": fruit_offering.dict(),
            "state": "OFFERED"
        }
        return { "Successfully notified the mobile press operator."}
    return {"Error": resp.text}


@router.get(
    "/farmer/fruitOfferings/",
    responses={
        200: {"model": List[FruitOfferingDB], "description": "List of fruit offerings of Farmers."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruit_offerings_get(
) -> List[FruitOfferingDB]:
    """List of fruit offerings."""
    return database.get_database_fruit_offerings()


@router.post(
    "/farmer/fruitRequests/",
    responses={
        200: {"description": "Successfully notified the farmer."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruit_request(
    fruit_request: FruitRequest = Body(None, description="The fruit containing at least a name, a quantity and a price."),
) -> None:
    """Ask for fruit at the farmer."""
    database.fruit_requests[fruit_request.id] = {
        "fruit_request": fruit_request.dict(),
        "state": "REQUESTED"
    }
    return {"description": "Successfully notified the farmer."}


@router.get(
    "/farmer/fruitRequests/",
    responses={
        200: {"model": List[FruitRequestDB], "description": "List of fruit requests of Food Banks."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruit_requests_get(
) -> List[FruitRequestDB]:
    """List of fruit requests."""
    return database.get_database_fruit_requests()


@router.get(
    "/farmer/fruits/",
    responses={
        200: {"model": List[FruitDB], "description": "List of all fruits the farmer has available."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruits_get(
) -> List[FruitDB]:
    """Get all fruits the farmer has available."""
    return database.get_database_fruits()


@router.post(
    "/farmer/fruits/",
    responses={
        200: {"description": "Fruits added."},
    },
    tags=["Farmer"],
    response_model_by_alias=True,
)
async def farmer_fruits_post(
    fruit_db: List[FruitDB] = Body(None, description=""),
) -> None:
    """Add fruits"""
    for fruit in fruit_db:
        database.fruits[fruit.id] = fruit.dict()
    return {"description": "Fruits added."}